import React, { useState, useEffect } from "react";
import axios from "axios";
import ReactApexChart from 'react-apexcharts';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from 'react-bootstrap';
import ReactDOM from 'react-dom';
import ReactPaginate from 'react-paginate';
import JSONPretty from 'react-json-pretty';
import 'react-json-pretty/themes/monikai.css';
import "./App.css";

// const api_url = "http://10.42.144.61:5000/api-volumes";
// const api_url = "http://127.0.0.1:5000/api-images";
// const api_url = "http://127.0.0.1:5000/api-servers";
const api_url = "http://127.0.0.1:5000/api";
//const api_url = "http://10.42.144.61:5000/api"


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      series: [],
      options: [],
      // testdata: this.getTestData(),
      offset: 0,
      data: [],
      perPage: 20,
      currentPage: 0
    };
    this.handlePageClick = this
      .handlePageClick
      .bind(this);
  }


  receivedData() {
    axios
      .get(api_url)
      .then(res => {
        const data = res.data;
        // const data = adata["test_cases"]
        const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)
        const lala = 1
        const postData = slice.map(pd => <div class="container"><React.Fragment>
          { }
          <Container >
            <div id="kontak">
              <Row>
                <Col>
                  {pd["instances"]["instance"]["name"]}
                </Col>
                <Col>Start: {pd["start"]}</Col>
                <Col>Finish: {pd["finish"]}</Col>
              </Row>
            </div>
            <div id="kontak2">
              <div>
                <div id={pd["id"]}>
                  <Row>
                    <Col>
                      <div id="chart">

                        <ReactApexChart options={pd["options"][0]} series={pd["series"]} type="rangeBar" height={250} />
                      </div>
                    </Col>
                    {/* <Col>
                    {
                  Array.isArray(pd["consumption"])    
                    ? pd["consumption"].map(cons => (
                      Array.isArray(cons["disk_gb"])
                      ? pd["consumption"]["disk_gb"].map((index,value) => (
                        Array.isArray(value)
                        ? value.map((item) => (item))
                        : <div><p>{value}</p>  <p>{pd["consumption"]["size_time"][index]}</p></div>))
                     : cons["disk_gb"]  
                    ))
                    : Array.isArray(pd["consumption"]["disk_gb"])
                      ? pd["consumption"]["disk_gb"].map((index,value) => (
                         Array.isArray(value)
                         ? value.map((item) => (item))
                         : <p>{value}</p>))
                      : <div>{pd["consumption"]["disk_gb"]}{pd["consumption"]["total_gb_h"]} </div>
                     }
                    
                    </Col> */}

                    <Col id="json_col">

                      <JSONPretty data={pd["consumption"]}></JSONPretty>
                    </Col>
                  </Row>
                  {



                  }
                  <Row>
                    <Col>
                      <table>
                        <thead >
                          <tr>
                            <th>       Events: </th>
                          </tr></thead>
                        <thead>
                          {
                            "volume" in pd["instances"]
                            ?  <tr>
                                <th >Event Name</th>
                                <th>created</th>
                                <th>bill_state</th>
                                <th>os_state</th>
                                <th>size_gb</th>
                                <th>storgae_type</th>
                              </tr>
                            : "server" in pd["instances"]
                              ? <tr>
                              <th >Event Name</th>
                              <th>created</th>
                              <th>bill_state</th>
                              <th>os_state</th>
                              <th>vcpu</th>
                              <th>mem</th>
                              <th>disk</th>
                              <th>flavor id</th>
                               </tr>
                               :<tr>
                               <th >Event Name</th>
                               <th>created</th>
                               <th>bill_state</th>
                               <th>os_state</th>
                               <th>size_gb</th>
                             </tr>
                                }
                        </thead>
                        <tbody>
                          {pd["tevents"].map(event => (
                            // <tr>
                            //   <th>{event["name"]}</th>
                            //   <th>{event["created"]}</th>
                            //   <th>{event["bill_state"]}</th>
                            //   <th>{event["os_state"]}</th>
                              
                                "volume" in pd["instances"]
                                ? <tr>
                                  <th>{event["name"]}</th>
                                  <th>{event["created"]}</th>
                                  <th>{String(event["bill_state"])}</th>
                                  <th>{event["os_state"]}</th>
                                  <th>{event["size_gb"]}</th>
                                  <th>{event["volume_type_name"]}</th>
                                  </tr>
                                : "server" in pd["instances"]
                                  ? <tr>
                                    <th>{event["name"]}</th>
                                    <th>{event["created"]}</th>
                                    <th>{String(event["bill_state"])}</th>
                                    <th>{event["os_state"]}</th>
                                    <th>{event["vcpu"]}</th>
                                    <th>{event["mem_gb"]}</th>
                                    <th>{event["disk_gb"]}</th>
                                    <th>{event["flavor_id"]}</th>
                                  </tr>
                                  : <tr>
                                    <th>{event["name"]}</th>
                                    <th>{event["created"]}</th>
                                    <th>{String(event["bill_state"])}</th>
                                    <th>{event["os_state"]}</th>
                                    <th>{event["size_gb"]}</th>
                                  </tr>
                              
                            // </tr>
                          ))}</tbody>
                      </table>
                    </Col>
                    <Col>

                      <table>
                        <thead>
                          <tr>
                            <th>Consumption Sum </th>
                          </tr></thead>
                        <thead>
                          {
                          "volume" in pd["instances"]
                          ? <tr>
                            <th >Storage Type</th>
                            <th >GB per H</th>
                          </tr>
                          : "server" in pd["instances"]
                            ? <tr>
                            <th >disk</th>
                            <th >vcpu per H</th>
                            <th >mem per H</th>
                          </tr>
                             : <tr>
                             <th >GB per H</th>
                           </tr>

                          }
                          
                        </thead>
                        <tbody>
                          {
                            "volume" in pd["instances"]
                              ? Array.isArray(pd["ncon"])
                                ? pd["ncon"].map(cons => (
                                <tr>
                                <th>{cons["volume_type_name"]}</th>
                                <th>{cons["total_h_gb"]}</th>
                                </tr>))
                                : <tr>
                                  <th>{pd["ncon"]["volume_type_name"]}</th>
                                  <th>{pd["ncon"]["total_h_gb"]}</th>
                                  </tr>
                              : "server" in pd["instances"]
                                // ? Array.isArray(pd["consumption"]["flavor"])
                                  ? 
                                  <tr>
                                  <th>{pd["consumption"]["disk_h_gb"]}</th>
                                  <th>{pd["consumption"]["vcpu_h"]}</th>
                                  <th>{pd["consumption"]["mem_h_gb"]}</th>
                                  </tr>
                                : <tr><th>{pd["consumption"]["disk_gbh"]}</th></tr>

                          }
                        </tbody>
                      </table>
                    </Col>
                  </Row>


                </div>

              </div>
            </div>
          </Container>

        </React.Fragment>
        </div>)
        this.setState({
          pageCount: Math.ceil(data.length / this.state.perPage),
          postData
        })
      });
  }

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState({
      currentPage: selectedPage,
      offset: offset
    }, () => {
      this.receivedData()
    });

  };

  componentDidMount() {
    this.receivedData()
  }

  render() {
    return (
      <div>
        {this.state.postData}
        <ReactPaginate
          previousLabel={"prev"}
          nextLabel={"next"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={this.state.pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={this.handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"} />
      </div>
    );
  }

}

