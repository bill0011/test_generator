import datetime
import pytz

class ServerTestFormatter:
    def format(self,test_data):
        test_code = '''from django.test import TestCase
from osbill.biller import ServerBiller
from osbill.models import Project, Server, ServerEvent, Flavor, BillCodeProjectRelation, BillCode
import datetime
import pytz

class ServerCalcTestCase(TestCase):
     
    maxDiff = None
    def setUp(self):
        bc = BillCode.objects.create( 
            id = {bcid}, 
            code = "{code}",
            active = {active})        
        project = Project.objects.create( 
            id = {id}, 
            openstack_id = "{openstack_id}",
            name = "{name}", 
            description = "{description}", 
            created = "{created}",
            enabled = {enabled})
        bcpr = BillCodeProjectRelation.objects.create( 
            id = {tid}, 
            billcode = BillCode.objects.get(id={bcid}),
            project = Project.objects.get(id={id}), 
            activated = "{activated}")
        Flavor.objects.bulk_create(
        ['''.format(
        id = test_data["project"]["id"],
        openstack_id = test_data["project"]["openstack_id"],
        name = test_data["project"]["name"],
        description = test_data["project"]["description"],
        created = test_data["project"]["created"],
        enabled = test_data["project"]["enabled"],
        bcid = test_data["bc"]["id"],
        code = test_data["bc"]["code"],
        active = test_data["bc"]["active"],
        tid = test_data["bcpr"]["id"],
        activated = test_data["bcpr"]["activated"],
        )

        flavors = ""
        for vt in test_data["flavors"]:
            line='''
        Flavor(
            id = {instance_id}, 
            openstack_id = '{openstack_id}', 
            name = '{name}', 
            ram_gb = '{ram_gb}', 
            disk_gb = '{disk_gb}', 
            vcpu={vcpu}),'''.format(
                instance_id=vt["id"],
                openstack_id=vt["openstack_id"],
                name=vt["name"],
                ram_gb=vt["ram_gb"],
                disk_gb=vt["disk_gb"],
                vcpu=vt["vcpu"],
            )
            flavors += line

        test_code += flavors

        test_code += '''
        ])
        Server.objects.bulk_create(
        ['''

        instances = ""
        for instance in [x["instances"] for x in test_data["test_cases"]]:
            line='''
        Server(
            id = {instance_id}, 
            openstack_id = "{openstack_id}", 
            name = "{name}", 
            project = Project.objects.get(id={project})),'''.format(
                instance_id=instance["instance"]["id"],
                openstack_id=instance["instance"]["openstack_id"],
                name=instance["instance"]["name"],
                project=instance["instance"]["project_id"],
            )
            instances+=line

        test_code += instances

        test_code += '''
        ])
        ServerEvent.objects.bulk_create(
        ['''
        events=""
        
        se = []
        for case in test_data["test_cases"]:
            for ev in case["events"]:
              se.append(ev)
        for index,event in enumerate(se):
            line='''
        ServerEvent(
            created = "{created}",
            name = "{name}", 
            server = Server.objects.get(id={server_id}), 
            bill_state = {bill_state}, 
            os_state = "{os_state}",
            flavor = Flavor.objects.get(id={vt_id}),
            vcpu = {vcpu},
            disk_gb = {disk_gb},
            ram_gb = {ram_gb}),'''.format(
                openstack_id=event["openstack_id"],
                created=event["created"],
                name=event["name"],
                server_id=event["server_id"],
                bill_state=event["bill_state"],
                os_state=event["os_state"],
                vt_id = event["flavor_id"],
                vcpu = event["vcpu"],
                ram_gb = event["ram_gb"],
                disk_gb = event['disk_gb']
            )
            events+=line
        test_code += events
        test_code += '''
        ])'''

        test_cases = ""
        for index,test_case in enumerate(test_data["test_cases"], 1):
            line = '''

    def test_calc_server{index}(self):
        start_time = datetime.datetime.strptime("{start}", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("{finish}", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id={project_id})
        bill_run = ServerBiller(start_time, finish_time, projects, "{openstack_id}")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {{            
            'instance_id': {instance_id},
            'instance_name': '{name}',
            'openstack_id': '{openstack_id}',
            'events': [],
            'license': 'NoLicense',
            'consumption': {consumption},                   
            'project': {project_id},
            'project_name': '{project_name}',
            'project_description': '{project_description}',
            'project_openstack_id': '{project_openstack_id}',
            'billing_code' : '{terp}'      
            }}
        self.assertEqual(bill_cons[0], test_controller)
         '''.format(
            project_id = test_data["project"]["id"],
            project_name = test_data["project"]["name"],
            project_description = test_data["project"]["description"],
            start = test_case["start"].split("+00")[0],
            finish = test_case["finish"].split("+00")[0],
            instance_id = test_case["instances"]["instance"]["id"],
            name = test_case["instances"]["instance"]["name"],
            openstack_id = test_case["instances"]["instance"]["openstack_id"],
            consumption = test_case["consumption"],         
            project_openstack_id = test_data["project"]["openstack_id"],
            index = index,
            terp=test_data["project"]["description"].split("- terp ")[1]
            )
            test_cases+=line
        test_code += test_cases

        return test_code
  