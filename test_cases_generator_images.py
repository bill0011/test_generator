from datetime import datetime, timedelta
import pytz
from test_cases_schema import image_test_schema
from chart_data import chart_data

class ImageTestGenerator:
    default_time = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) +  timedelta(days=1)
    start_time = default_time
    finish_time = default_time

    def finisher(case, created):
        if case == "f":
            ImageTestGenerator.finish_time = created
            return ImageTestGenerator.finish_time
        else:
            return 0

    def starter(case, created):
        if case == "s":
            ImageTestGenerator.start_time = created
            return ImageTestGenerator.start_time
        else:
            return 0

    def image_test_gen(self):
        bcode = {
            'id' : 1, 
            'code': '20986.91.04',
            'active': True,
        }
        bcpr = {
            'id' : 1, 
            'billcode_id': 1,
            'project_id': 1, 
            'activated': datetime.strptime('01-01-2021T00:00:00.0 +0000',
                                   '%d-%m-%YT%H:%M:%S.%f %z').replace(tzinfo=pytz.utc),
            }
        project = {
        "id": 1,
        "openstack_id": 'd670223972c2479ebcc5c7924e9f4a2e',
        "name": "test-project",
        "description": 'test project - terp 20986.91.04',
        "created": '2020-01-01 00:00:00.000000+00',
        "enabled": True
        }
        schema = image_test_schema()
        event_id = 1
        image_id = 0
        test_set = {}
        test_list=[]
        images=[]
        for sch in schema:
            events = []
            tevents = []
            created = ImageTestGenerator.default_time
            image_id += 1
            image = {"image": True,
                "instance": {
                "id": image_id,
                "openstack_id": 'test-image-id-'+ str(image_id),
                "name": 'test_image_'+ str(image_id),
                "project_id": project["id"]
                }}
            size = 0
            run_time=0
            disk_gbh=0
            started = False
            finished = False
            bc = False
            bd = False
            for s in sch:
                created += timedelta(hours=1)
                created_str = created.strftime('%Y-%m-%d %H:%M:%S')
                test_case = {}
                switcher = {
                    "s": ImageTestGenerator.starter(s, created_str),
                    "f": ImageTestGenerator.finisher(s, created_str),
                    "c": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'create',
                        "image_id": image_id,
                        "bill_state": True,
                        "os_state": 'active',
                        "size_gb": 10
                    },
                    "d": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'delete',
                        "image_id": image_id,
                        "bill_state": False,
                        "os_state": 'deleted',
                        "size_gb": 10
                    },
                    "i": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'image_size_update',
                        "image_id": image_id,
                        "bill_state": True,
                        "os_state": 'active',
                        "size_gb": 10
                    },
                }
                if s == "d": 
                    bd = True
                    ev=switcher.get(s, "switcher error, key: "+str(s))
                    events.append(ev)
                    tevents.append(ev)
                    
                    event_id += 1
                elif s == "c": 
                    bc = True
                    event_id += 1                    
                    ev=switcher.get(s, "switcher error, key: "+str(s))
                    size = ev["size_gb"]
                    events.append(ev)
                    tevents.append(ev)
                    if started and not finished:
                        run_time += 1
                        disk_gbh += ev["size_gb"]
                        
                elif s == "s":
                    tev={
                        "created": created_str,
                        "name": 'START',
                        "bill_state": " "
                    }
                    tevents.append(tev)
                    started = True
                    if bc and not bd:
                        run_time += 1
                        disk_gbh += 10

                elif s == "f":
                    tev={
                        "created": created_str,
                        "name": 'FINISH',
                        "bill_state": " "
                    }
                    tevents.append(tev)
                    finished = True

                else:
                    event_id += 1
                    ev=switcher.get(s, "switcher error, key: "+str(s))
                    size = ev["size_gb"]
                    events.append(ev)
                    tevents.append(ev)
                    if started and not finished and not bd:
                        run_time += 1
                        disk_gbh += ev["size_gb"]  
            test_case = {
                "start": ImageTestGenerator.start_time,
                "finish": ImageTestGenerator.finish_time,
                "instances": image,
                "events": events,
                "tevents": tevents,
                "disk_gbh": disk_gbh,
                "consumption": {"disk_gbh": disk_gbh},
                "ncon": {"disk_gbh": disk_gbh},
                "run_time" : run_time,             
                "size": size
                }
            test_list.append(chart_data(test_case))
        test_set = {
                "project": project,
                "test_cases": test_list,
                "bcpr": bcpr,
                "bc": bcode
            }
        return test_set