from flask import Flask, send_from_directory
from flask import jsonify
from flask import json
from test_cases_generator_images import ImageTestGenerator
from test_cases_formater_images import ImageTestFormatter
from test_cases_generator_volumes import VolumeTestGenerator
from test_cases_formater_volumes import VolumeTestFormatter
from test_cases_generator_servers import ServerTestGenerator
from test_cases_formater_servers import ServerTestFormatter
import json
from flask_cors import CORS, cross_origin


app = Flask(__name__, static_url_path='', static_folder='frontend/build')
CORS(app, support_credentials=True)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route("/", defaults={'path':''})
def serve(path):
    return send_from_directory(app.static_folder,'index.html')

@app.route('/images')
def images():
    tg = ImageTestGenerator()
    events = tg.image_test_gen()
    tf=ImageTestFormatter()
    code = tf.format(events)
    response = app.response_class(
        response=code,
        status=200,
        mimetype='text'
    )
    return response


@app.route('/api-images')
def apiimages():
    tg = ImageTestGenerator()
    events = tg.image_test_gen()
    # tf=ImageTestFormatter()
    # code = tf.format(events)
    response = app.response_class(
        response=json.dumps(events, default=str),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/volumes')
def volumes():
    tg = VolumeTestGenerator()
    events = tg.volume_test_gen()
    tf=VolumeTestFormatter()
    code = tf.format(events)
    response = app.response_class(
        response=code,
        status=200,
        mimetype='text'
    )
    return response

@app.route('/api-volumes')
def apivolumes():
    tg = VolumeTestGenerator()
    events = tg.volume_test_gen()
    # tf=VolumeTestFormatter()
    # code = tf.format(events)
    response = app.response_class(
        response=json.dumps(events, default=str),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/servers')
def servers():
    tg = ServerTestGenerator()
    events = tg.server_test_gen()
    tf=ServerTestFormatter()
    code = tf.format(events)
    response = app.response_class(
        response=code,
        status=200,
        mimetype='text'
    )
    return response

@app.route('/api-servers')
# @cross_origin(support_credentials=True)
def apiservers():
    tg = ServerTestGenerator()
    events = tg.server_test_gen()
    # tf=ServerTestFormatter()
    # code = tf.format(events)
    response = app.response_class(
        response=json.dumps(events, default=str),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/api')
# @cross_origin(support_credentials=True)
def api():
    tg = ServerTestGenerator()
    vg = VolumeTestGenerator()
    ig = ImageTestGenerator()
    sevents = tg.server_test_gen()
    vevents = vg.volume_test_gen()
    ievents = ig.image_test_gen()
    ev = []
    ev.extend(sevents["test_cases"])
    ev.extend(vevents["test_cases"])
    ev.extend(ievents["test_cases"])
    response = app.response_class(
        response=json.dumps(ev),
        status=200,
        mimetype='application/json'
    )
    return response