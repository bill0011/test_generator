from datetime import datetime, timedelta
import pytz
from test_cases_schema import volume_test_schema
from chart_data import chart_data


class VolumeTestGenerator:
    default_time = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) +  timedelta(days=1)
    start_time = default_time
    finish_time = default_time  
    
    def finisher(case, created):
        if case == "f":
            VolumeTestGenerator.finish_time = created
            return VolumeTestGenerator.finish_time
        else:
            return 0

    def starter(case, created):
        if case == "s":
            VolumeTestGenerator.start_time = created
            return VolumeTestGenerator.start_time
        else:
            return 0

    def volume_test_gen(self):
        bcode = {
            'id' : 1, 
            'code': '20986.91.04',
            'active': True,
        }
        bcpr = {
            'id' : 1, 
            'billcode_id': 1,
            'project_id': 1, 
            'activated': '2020-01-01 00:00:00+00:00'
            }
        project = {
            "id": 1,
            "openstack_id": 'd670223972c2479ebcc5c7924e9f4a2e',
            "name": "test-project",
            "description": 'test project - terp 20986.91.04',
            "created": '2020-01-01 00:00:00.000000+00',
            "enabled": True
        }
        volume_type1 = {
            "id": 0,
            "openstack_id": 'd670223972c2479ebcc5c7924e9f4a2r',
            "name": "standard",
            "is_public": True
        }
        volume_type2 = {
            "id": 1,
            "openstack_id": 'd670223972c2479ebcc5c7924e9f4a2q',
            "name": "performance",
            "is_public": True
        }
        volume_type3 = {
            "id": 2,
            "openstack_id": 'd670223972c2479ebcc5c7924e9f4a2w',
            "name": "performance-20000",
            "is_public": True
        }
        volume_types = [volume_type1, volume_type2, volume_type3]
        schema = volume_test_schema()
        event_id = 1
        volume_id = 0
        test_set = {}
        test_list = []

        for sch in schema:
            events = []
            tevents = []
            created = VolumeTestGenerator.default_time
            volume_id += 1
            volume = {"volume": True,
                      "instance": {
                          "id": volume_id,
                          "openstack_id": 'test-volume-id-' + str(volume_id),
                          "name": 'test_volume_' + str(volume_id),
                          "project_id": project["id"],
                      }
                      }
            vt_id = 0
            total_h_gb = 0
            size_time = 0
            # disk_gb = 0
            disk_gbh = 0.0
            started = False
            finished = False
            bc = False
            bd = False
            retyped = False
            resize_counter = 1.0
            resize_time = []
            s_time = 0.0
            resize_gb = []
            s_size = 10.0
            resize_retype = []
            resize_retype_id = []
            s_type = "standard"
            s_type_id = 0
            retyped_consumption = []
            consumption = []
            test_case = {}
            for s in sch:
                if s == "t":
                    if vt_id >= 2:
                        vt_id = 0
                    else:
                        vt_id += 1
                created += timedelta(hours=1)
                created_str = created.strftime('%Y-%m-%d %H:%M:%S+00:00')

                switcher = {
                    "s": VolumeTestGenerator.starter(s, created_str),
                    "f": VolumeTestGenerator.finisher(s, created_str),
                    "c": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'create',
                        "volume_id": volume_id,
                        "bill_state": True,
                        "os_state": 'active',
                        "size_gb": resize_counter * 10,
                        "volume_type_name": volume_types[vt_id]["name"],
                        "volume_type_id": volume_types[vt_id]["id"],
                    },
                    "d": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'delete',
                        "volume_id": volume_id,
                        "bill_state": False,
                        "os_state": 'deleted',
                        "size_gb": resize_counter * 10,
                        "volume_type_name": volume_types[vt_id]["name"],
                        "volume_type_id": volume_types[vt_id]["id"],
                    },
                    "r": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'resize',
                        "volume_id": volume_id,
                        "bill_state": True,
                        "os_state": 'resized',
                        "size_gb": resize_counter * 10,
                        "volume_type_name": volume_types[vt_id]["name"],
                        "volume_type_id": volume_types[vt_id]["id"],
                    },
                    "t": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'retype',
                        "volume_id": volume_id,
                        "bill_state": True,
                        "os_state": 'retyped',
                        "size_gb": resize_counter * 10,
                        "volume_type_name": volume_types[vt_id]["name"],
                        "volume_type_id": volume_types[vt_id]["id"],
                    }}
                if s == "d":
                    bd = True
                    ev = switcher.get(s, "switcher error, key: "+str(s))
                    events.append(ev)
                    tevents.append(ev)
                    event_id += 1
                    if resize_time and bc and started and not finished:
                        resize_gb.append(s_size)
                        resize_time.append(s_time)
                        resize_retype.append(s_type)
                        resize_retype_id.append(s_type_id)
                    if retyped  and bc and started and not finished:
                        resize_gb.append(s_size)
                        resize_time.append(s_time)
                        resize_retype.append(s_type)
                        resize_retype_id.append(s_type_id)

                    if not started:
                        s_type = ev["volume_type_name"]
                        s_type_id = ev["volume_type_id"]
                        s_size = ev["size_gb"]

                elif s == "c":
                    bc = True
                    event_id += 1
                    ev = switcher.get(s, "switcher error, key: "+str(s))
                    events.append(ev)
                    tevents.append(ev)
                    if started and not finished:
                        disk_gbh = ev["size_gb"]
                        s_time = 1.0
                        s_type = ev["volume_type_name"]
                        s_type_id = ev["volume_type_id"]
                        s_size = ev["size_gb"]

                elif s == "s":
                    tev = {
                        "created": created_str,
                        "name": 'START',
                        "bill_state": " ",
                        "volume_type_name": " ",
                        "volume_type_id": " ",
                    }
                    tevents.append(tev)
                    started = True
                    if bc and not bd:
                        disk_gbh = resize_counter * 10
                        s_time = 1.0
                        s_size = resize_counter * 10
                        s_type = volume_types[vt_id]["name"]
                        s_type_id = volume_types[vt_id]["id"]
                elif s == "f":
                    tev = {
                        "created": created_str,
                        "name": 'FINISH',
                        "bill_state": " ",
                        "volume_type_name": " ",
                        "volume_type_id": " ",
                    }
                    tevents.append(tev)
                    finished = True
                    if resize_time and bc and not bd:
                        resize_gb.append(s_size)
                        resize_time.append(s_time)
                        resize_retype.append(s_type)
                        resize_retype_id.append(s_type_id)

                elif s == "r":
                    ev=switcher.get(s, "switcher error, key: "+str(s))
                    events.append(ev)
                    event_id += 1
                    if bc and started and not bd and not finished:
                        resize_gb.append(resize_counter * 10)
                        resize_time.append(s_time)
                        resize_retype.append(s_type)
                        resize_retype_id.append(s_type_id)
                        disk_gbh += resize_counter * 10
                        s_type = volume_types[vt_id]["name"]
                        s_type_id = volume_types[vt_id]["id"]
                        s_size = resize_counter * 10
                        s_time = 1.0
                    # event_id += 1

                elif s == "t":

                    ev = switcher.get(s, "switcher error, key: "+str(s))
                    events.append(ev)
                    tevents.append(ev)
                    event_id += 1
                    if bc and started and not bd and not finished:
                        resize_gb.append(resize_counter * 10)
                        resize_time.append(s_time)
                        resize_retype.append(s_type)
                        resize_retype_id.append(s_type_id)
                        disk_gbh += resize_counter * 10
                        s_type = volume_types[vt_id]["name"]
                        s_type_id = volume_types[vt_id]["id"]
                        s_size = resize_counter * 10
                        s_time = 1.0

            if resize_time:
                for index, item in enumerate(resize_time):
                    total_h_gb += resize_time[index] * resize_gb[index]
                consumption = {
                    "volume_type_name": resize_retype,
                    "volume_type": resize_retype_id,
                    "total_h_gb": total_h_gb,
                    "disk": resize_gb,
                    "size_time": resize_time
                }
            else:
                consumption = {
                    "volume_type_name": [s_type],
                    "volume_type": [s_type_id],
                    "total_h_gb": disk_gbh,
                    "disk": [s_size],
                    "size_time": [s_time]
                }
            
            if True: 
                standart = 0.0
                performance = 0.0
                per20000 = 0.0
                for c in [consumption]:
                    if isinstance(c["volume_type_name"], list):
                        for index, st in enumerate(c["volume_type_name"]):
                            if st == "standard":
                                standart += c["disk"][index] * c["size_time"][index]
                            elif st == "performance":
                                performance += c["disk"][index] * c["size_time"][index]
                            elif st == "performance-20000":
                                per20000 += c["disk"][index] * c["size_time"][index]
                    elif c["volume_type_name"] == "standard":
                        standart += c["total_h_gb"]
                    elif c["volume_type_name"] == "performance":
                        performance += c["total_h_gb"]
                    elif c["volume_type_name"] == "performance-20000":
                        per20000 += c["total_h_gb"]
                ncon = [
                    {"volume_type_name": "standard", "total_h_gb": standart},
                    {"volume_type_name": "performance", "total_h_gb": performance},
                    {"volume_type_name": "performance-20000", "total_h_gb": per20000},
                ]
                consumption["total_h_gb"]={}
                if standart:
                    consumption["total_h_gb"]["standard_volume_type"] = standart
                if performance:
                    consumption["total_h_gb"]["performance_volume_type"] = performance
                if per20000:
                    consumption["total_h_gb"]["performance-20000_volume_type"] = per20000
               
            test_case = {
                "start": VolumeTestGenerator.start_time,
                "finish": VolumeTestGenerator.finish_time,
                "instances": volume,
                "events": events,
                "tevents": tevents,
                "consumption": consumption,
                "ncon": ncon
            }

            test_list.append(chart_data(test_case))
        test_set = {
            "project": project,
            "volume_types": volume_types,
            "test_cases": test_list,
            "bcpr": bcpr,
            "bc": bcode
        }
        return test_set
