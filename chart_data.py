from datetime import datetime, timedelta
import pytz
from flask import json
from flask import jsonify


def chart_data(test_case):
    data = {}
    # options = {}
    billed_events = []
    nbilled_events = []
    billed = {"name": "Billed"}
    nbilled = {"name": "Not Billed"}
    bi = {"name": "Billing Interval"}
    start = datetime.fromisoformat(
        test_case["start"].split("+")[0])
    finish = datetime.fromisoformat(
        test_case["finish"].split("+")[0])
    options = {
        "chart": {
            # "offsetX": 0,
            # "selection": {
            #     "enabled": True,
            #     "type": 'x',
            #     "fill": {
            #         "color": '#24292e',
            #         "opacity": 0.1
            #     },
            #     "stroke": {
            #         "width": 1,
            #         "dashArray": 3,
            #         "color": '#24292e',
            #         "opacity": 0.4
            #     },
            #     "xaxis": {
            #         # int(test_case["start"].split("01-01 ")[1].split(":")[0]),
            #         "min": 1,
            #         # int(test_case["finish"].split("01-01 ")[1].split(":")[0])
            #         "max": 2
            #     },
            #     "yaxis": {
            #         "min": 0,
            #         "max": 1
            #     }
            # },
            "height": 450,
            "type": 'rangeBar'
        },
        "grid": {
            "show": True,
            "borderColor": '#90A4AE',
            "strokeDashArray": 0,
            "position": 'back',
            "xaxis": {
                "lines": {
                    "show": True
                }
            },
            "yaxis": {
                "lines": {
                    "show": True
                }
            },
            "row": {
                "colors": '#50A4AE',
                "opacity": 0.5
            },
            "column": {
                "colors": '#10A4AE',
                "opacity": 0.5
            },
            "padding": {
                "top": 0,
                "right": 0,
                "bottom": 0,
                "left": 0
            },
        },
        "plotOptions": {
            "bar": {
                "horizontal": True,
                "barHeight": '80%'
            }
        },
        "xaxis": {
            "type": 'numeric',
            "tickAmount": (len(test_case["events"]) + 2)
        },
        "yaxis": {
            # "type": 'category',
            "categories": ["Billed", "Not Billed"]
        },
        "stroke": {
            "width": 1
        },
        "fill": {
            "type": 'solid',
            "opacity": 0.6
        },
        "legend": {
            "position": 'top',
            "horizontalAlign": 'left'
        }
    },
    for ev in test_case["events"]:
        if not(ev["name"] in ["START", "FINISH"]):
            end = datetime.strptime(ev["created"].split(
                "+")[0], '%Y-%m-%d %H:%M:%S') + timedelta(hours=1)
            if end == start and (end + timedelta(hours=1) == finish): 
                end += timedelta(hours=2)
            elif end == start or end == finish:
                end += timedelta(hours=1)
            if ev["bill_state"]:
                bil = "Billed"
            else:
                bil = ""
            data = {
                "x": "",
                "y": [
                    int(ev["created"].split(" ")[1].split(":")[0]),
                    int(datetime.strftime(
                        end, '%Y-%m-%d %H:%M:%S').split(" ")[1].split(":")[0]),
                ]
            }
            if ev["bill_state"]:
                billed_events.append(data)
            else:
                nbilled_events.append(data)
    updated_case = test_case
    billed["data"] = billed_events
    nbilled["data"] = nbilled_events
    bi["data"] = [{
        "x": "Billing Interval",
        "y": [
            int(test_case["start"].split(" ")[1].split(":")[0]),
            int(test_case["finish"].split(" ")[1].split(":")[0]),
        ]}]

    updated_case["series"] = []
    if nbilled_events:
        updated_case["series"].append(nbilled)
    if billed_events:
        updated_case["series"].append(billed)
    updated_case["series"].append(bi)
    updated_case["options"] = options
    return updated_case
