from datetime import datetime, timedelta
import pytz
from test_cases_schema import server_test_schema
from chart_data import chart_data

class ServerTestGenerator:
    default_time = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) +  timedelta(days=1)
    start_time = default_time
    finish_time = default_time

    def finisher(case, created):
        if case == "f":
            ServerTestGenerator.finish_time = created
            return ServerTestGenerator.finish_time
        else:
            return 0

    def starter(case, created):
        if case == "s":
            ServerTestGenerator.start_time = created
            return ServerTestGenerator.start_time
        else:
            return 0

    def server_test_gen(self):
        bcode = {
        "id": 1, 
        "code": '20986.91.04',
        "active": True,
        }
        bcpr = {
        'id' : 1,  
        'activated': '2020-01-01 00:00:00+00:00'
        }
        project = {
        "id": 1,
        "openstack_id": 'd670223972c2479ebcc5c7924e9f4a2e',
        "name": "test-project",
        "description": 'test project - terp 20986.91.04',
        "created": '2020-01-01 00:00:00.000000+00',
        "enabled": True
        }
        flavor1={
        "id": 0,
        "openstack_id": 'd670223972c2479ebcc5c7924e9f4a2r',
        "name": "1-1-0",
        "vcpu" : 1,
        "ram_gb" : 1,
        "disk_gb" : 0
        }
        flavor2= {
        "id": 1,
        "openstack_id": 'd670223972c2479ebcc5c7924e9f4a2q',
        "name": "2-2-0",
        "vcpu" : 2,
        "ram_gb" : 2,
        "disk_gb" : 0
        }
        flavor3= {
        "id": 2,
        "openstack_id": 'd670223972c2479ebcc5c7924e9f4a2w',
        "name": "3-3-10",
        "vcpu" : 3,
        "ram_gb" : 3,
        "disk_gb" : 10
        }
        flavors = [flavor1, flavor2, flavor3]        
        schema = server_test_schema()
        event_id = 1
        server_id = 0
        test_set = {}
        test_list=[]
        
        shelved = [{"os_state": "shelve_offload", "bill_state": False},
                  {"os_state": "unshelve", "bill_state": True}]
        for sch in schema:
            events = []
            tevents = []
            created = ServerTestGenerator.default_time
            server_id += 1
            server = {"server": True,
                "instance": {
                "id": server_id,
                "openstack_id": 'test-server-id-'+ str(server_id),
                "name": 'test_server_'+ str(server_id),
                "project_id": project["id"]
                }}
            shelved_index=0
            resize_index = 0
            run_time = 0.0
            vcpu = 0.0
            mem = 0.0
            disk = 0.0
            flavor = 0
            started = False
            finished = False
            bc = False
            bd = False
            resize_mem=[]
            resize_vcpu=[]
            resize_time=[]   
            resize_flavors=[] 
            resize_disk= []        
            for s in sch:
                created += timedelta(hours=1)
                created_str = created.strftime('%Y-%m-%d %H:%M:%S+00:00')
                test_case = {}
                switcher = {
                    "s": ServerTestGenerator.starter(s, created_str),
                    "f": ServerTestGenerator.finisher(s, created_str),
                    "c": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'create',
                        "server_id": server_id,
                        "bill_state": True,
                        "os_state": 'active',
                        "flavor_id": flavors[resize_index]["id"],
                        "vcpu":flavors[resize_index]["vcpu"],
                        "ram_gb":flavors[resize_index]["ram_gb"],
                        "disk_gb": flavors[resize_index]["disk_gb"],
                    },
                    "d": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'delete',
                        "server_id": server_id,
                        "bill_state": False,
                        "os_state": 'deleted',
                        "flavor_id": flavors[resize_index]["id"],
                        "vcpu":flavors[resize_index]["vcpu"],
                        "ram_gb":flavors[resize_index]["ram_gb"],
                        "disk_gb": flavors[resize_index]["disk_gb"],
                    },
                    "r": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'resize',
                        "server_id": server_id,
                        "bill_state": True,
                        "os_state": 'active',
                        "flavor_id": flavors[resize_index]["id"],
                        "vcpu": flavors[resize_index]["vcpu"],
                        "ram_gb": flavors[resize_index]["ram_gb"],
                        "disk_gb": flavors[resize_index]["disk_gb"],
                    },
                    "u": {
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": shelved[shelved_index]["os_state"],
                        "server_id": server_id,
                        "bill_state": shelved[shelved_index]["bill_state"],
                        "os_state": shelved[shelved_index]["os_state"],
                        "flavor_id": flavors[resize_index]["id"],
                        "vcpu":flavors[resize_index]["vcpu"],
                        "ram_gb":flavors[resize_index]["ram_gb"],
                        "disk_gb": flavors[resize_index]["disk_gb"],
                    },
                }
                if s == "d": 
                    bd = True
                    ev=switcher.get(s, "switcher error, key: "+str(s))
                    events.append(ev)
                    tevents.append(ev)
                    event_id += 1
                    if resize_mem and bc and started and not finished:       
                            
                        resize_mem.append(float(flavors[resize_index]["ram_gb"]))
                        resize_vcpu.append(float(flavors[resize_index]["vcpu"]))
                        resize_disk.append(float(flavors[resize_index]["disk_gb"]))
                        resize_time.append(float(run_time))
                        resize_flavors.append(flavors[resize_index]["name"])
                

                elif s == "c": 
                    bc = True
                    event_id += 1                
                    ev=switcher.get(s, "switcher error, key: "+str(s))
                    events.append(ev)
                    tevents.append(ev)
                        
                    flavor_i = resize_index
                    if started and not finished:
                        run_time = 1
                        vcpu = ev["vcpu"]
                        mem = ev["ram_gb"]
                        disk = flavors[resize_index]["disk_gb"]
                        
                elif s == "s":
                    tev={
                        "created": created_str,
                        "name": 'START',
                        "bill_state": " "
                    }
                    tevents.append(tev)
                    started = True
                    flavor_i = resize_index
                    if bc and not bd and shelved_index == 0:                        
                        run_time = 1
                        vcpu = ev["vcpu"]
                        mem = ev["ram_gb"]
                        disk = flavors[resize_index]["disk_gb"]
                        
                elif s == "f":
                    tev={
                        "created": created_str,
                        "name": 'FINISH',
                        "bill_state": " "
                    }
                    tevents.append(tev)
                    finished = True
                    if resize_mem and bc and started and not bd:       
                                   
                        resize_mem.append(float(flavors[resize_index]["ram_gb"]))
                        resize_vcpu.append(float(flavors[resize_index]["vcpu"]))
                        resize_disk.append(float(flavors[resize_index]["disk_gb"]))
                        resize_time.append(float(run_time))
                        resize_flavors.append(flavors[resize_index]["name"])
                    
                elif s == "r": 
                    if bc and started and not bd and not finished:
                        resize_mem.append(float(flavors[resize_index]["ram_gb"]))
                        resize_vcpu.append(float(flavors[resize_index]["vcpu"]))
                        resize_disk.append(float(flavors[resize_index]["disk_gb"]))
                        resize_time.append(float(run_time))
                        resize_flavors.append(flavors[resize_index]["name"])

                    resize_index += 1
                    if resize_index >= 3:
                        resize_index=0
                    if bc and started and not bd and not finished:                   
                        run_time = 1.0
                        mem = ev["ram_gb"]
                        vcpu = ev["vcpu"]
                        disk = flavors[resize_index]["disk_gb"]
                        flavor_i = resize_index  
                    ev={
                        "openstack_id": event_id,
                        "created": created_str,
                        "name": 'resize',
                        "server_id": server_id,
                        "bill_state": True,
                        "os_state": 'active',
                        "flavor_id": flavors[resize_index]["id"],
                        "vcpu": flavors[resize_index]["vcpu"],
                        "ram_gb": flavors[resize_index]["ram_gb"],
                        "disk_gb": flavors[resize_index]["disk_gb"],
                    }
                    events.append(ev) 
                    tevents.append(ev)
                    event_id += 1           
                    
                elif s == "u":
                    event_id += 1
                    ev=switcher.get(s, "switcher error, key: "+str(s))
                    events.append(ev)       
                    tevents.append(ev)             
                    if started and not finished and not bd and shelved_index == 1:
                        flavor_i = resize_index
                        run_time += 1
                        vcpu += ev["vcpu"]
                        mem += ev["ram_gb"]
                        disk += flavors[resize_index]["disk_gb"]
                    if shelved_index >= 1:
                        shelved_index = 0
                    else:
                        shelved_index += 1

            if resize_mem:
                vcpu_h = 0.0
                mem_h_gb = 0.0
                disk_h_gb = 0.0
                run_timer = 0.0
                compute_type = []
                for index, item in enumerate(resize_mem):
                    vcpu_h += resize_time[index] * resize_vcpu[index]
                    mem_h_gb += resize_time[index] * resize_mem[index]
                    disk_h_gb += resize_time[index] * resize_disk[index]
                    run_timer += resize_time[index]
                    compute_type.append('default')
                consumption = {
                    "flavor": resize_flavors,
                    "vcpu_h": vcpu_h,
                    "mem_h_gb": mem_h_gb,
                    "run_time": run_timer,
                    "size_time": resize_time,
                    "disk_h_gb": disk_h_gb,
                    'compute_type': compute_type,
                    "vcpu": resize_vcpu,
                    "ram": resize_mem,
                    "disk": resize_disk,
                }    
            else:
                consumption = {                   
                    "flavor": [flavors[flavor_i]["name"]],
                    "vcpu_h": float(run_time) * flavors[flavor_i]["vcpu"],
                    "mem_h_gb": float(run_time) * flavors[flavor_i]["ram_gb"],
                    "size_time": [float(run_time)],
                    "run_time": float(run_time),
                    "disk_h_gb": disk,
                    'compute_type': ['default'],
                    "vcpu": [float(flavors[flavor_i]["vcpu"])],
                    "ram": [float(flavors[flavor_i]["ram_gb"])],
                    "disk": [float(flavors[flavor_i]["disk_gb"])],
                }    
            test_case = {
                "start": ServerTestGenerator.start_time,
                "finish": ServerTestGenerator.finish_time,
                "instances": server,
                "events": events,
                "tevents": tevents,
                "consumption": consumption,
                "ncon": consumption
                }
            test_list.append(chart_data(test_case))
        test_set = {
                "project": project,
                "flavors": flavors,
                "test_cases": test_list,
                "bcpr": bcpr,
                "bc": bcode
            }
        return test_set