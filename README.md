# README #

This Flask app generates tests for django billing application made for openstack billing

### What does it produce? ###

* Formated tests code for Openstack billing core algorithm
* Graphical output in react to visualize test cases and check/debug them easily
* Data json api that is use for GUI and tests generating.

### How do I get set up? ###

* clone, make sure python 3.8, pip, pipenv and npm and available on your system
* run command pipenv install
* run command pipenv shell
* in frontend folder run command npm run build (you may need to run npm install react-scripts --save first)
* in root folder run command flask run

### Site map ###

/ - GUI  
/images - tests for images  
/servers - tests for servers  
/volumes - tests for volumes  
/api-images - json image test formatters  
/api-volumes - json volume test formatters  
/api-servers - json server test formatters  
/api - json for GUI  